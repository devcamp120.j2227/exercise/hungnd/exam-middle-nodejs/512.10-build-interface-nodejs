// khai báo thư viện express
const express = require('express');

// Import timeCurrentMiddleware
const timeCurrentMiddleware = require('../middlewares/timeCurrentMiddleware');

//Tạo timeCurrentRouter
const timeCurrentRouter = express.Router();

// Print time current
timeCurrentRouter.get('/', timeCurrentMiddleware.getTimeCurrent);

module.exports = { timeCurrentRouter };