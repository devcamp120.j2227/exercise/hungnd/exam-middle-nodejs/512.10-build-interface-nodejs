// khai báo thư viện express
const express = require('express');

// Import urlRequestMiddleware
const urlRequestMiddleware = require('../middlewares/urlRequestMiddleware');

//Tạo urlRequestRouter
const urlRequestRouter = express.Router();

// Print url Request
urlRequestRouter.get('/', urlRequestMiddleware.getUrlRequest);

module.exports = { urlRequestRouter };