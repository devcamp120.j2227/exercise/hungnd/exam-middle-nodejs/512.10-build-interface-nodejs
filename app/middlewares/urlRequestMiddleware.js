// Middleware in ra url request
const getUrlRequest = (req, res, next) => {
    let url = req.headers.host;
    console.log("Url: ", url);
    next();
};

module.exports = {
    getUrlRequest
}