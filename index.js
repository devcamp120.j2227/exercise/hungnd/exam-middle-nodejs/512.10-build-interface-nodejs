// Import thư viện expressjs tương đương import express from "express";
const express = require("express");

// Import thư viện path
const path = require("path");

// Khởi tạo 1 app express
const app = express();

// khai báo cổng chạy project
const port = 8000;

// Khai báo để sử dụng bodyJson
app.use(express.json());

// Khai báo để sử dụng UTF-8
app.use(express.urlencoded({
    extended: true
}));

// Import timeCurrentRouter và sử dụng
const { timeCurrentRouter } = require('./app/routes/timeCurrentRouter');
app.use('/', timeCurrentRouter);

// Import urlRequestRouter và sử dụng
const { urlRequestRouter } = require("./app/routes/urlRequestRouter");
app.use('/', urlRequestRouter)

// Khai báo để sử dụng tài nguyên tĩnh
app.use(express.static(__dirname + "/views"));

// Show mockup
app.use('/', (req, res) => {
    res.sendFile(path.join(__dirname + "/views/Signin Form.html"));
});

app.listen(port, () => {
    console.log("App running on port: ", port);
});